# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/Camera.cpp" "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/bin/CMakeFiles/13_HelloMoon.dir/Camera.cpp.obj"
  "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/HelloMoon.cpp" "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/bin/CMakeFiles/13_HelloMoon.dir/HelloMoon.cpp.obj"
  "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/Shader.cpp" "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/bin/CMakeFiles/13_HelloMoon.dir/Shader.cpp.obj"
  "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/Sphere.cpp" "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/bin/CMakeFiles/13_HelloMoon.dir/Sphere.cpp.obj"
  "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/Texture.cpp" "C:/Users/Admin/Desktop/Repositories/gpr5300-graphics-and-shader-programming-for-games/13_HelloMoon/bin/CMakeFiles/13_HelloMoon.dir/Texture.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "D:/Libraries/SDL2-2.0.9/x86_64-w64-mingw32/include"
  "D:/Libraries/glew-2.1.0/include"
  "D:/Libraries/glm-0.9.9.4/include"
  "D:/Libraries/FreeImage-3.18.0/Dist/x64"
  "D:/Libraries/FreeImage-3.18.0/Wrapper/FreeImagePlus/dist/x64"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
