#version 440

in vec4 out_color;
in vec2 out_uv;

uniform sampler2D sampler;

out vec4 fragColor;

void main() {
    vec4 texel = texture(sampler, out_uv);

    fragColor = texel;
}
