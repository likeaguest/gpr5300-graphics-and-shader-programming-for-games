#version 440

in vec3 in_position;
in vec3 in_color;
in vec2 in_uv;

uniform mat4 mvp;

out vec4 out_color;
out vec2 out_uv;

void main() {

    out_color = vec4(in_color, 1.0);

    out_uv = in_uv;

    gl_Position = mvp * vec4(in_position, 1.0);

}
