/**
 * 03_SDLimage - Image loading and rendering with SDL
 */

#include <iostream>
#include <SDL2/SDL.h>

#ifdef __APPLE__
#include <SDL2_image/SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

std::string progName = "03_SDLimage";

// Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Window pointer (handler)
SDL_Window* window = nullptr;

// Window surface pointer
SDL_Surface* screenSurface = nullptr;

// Image pointer
SDL_Surface* image = nullptr;

bool init() {
    // Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        success = false;
    } else {

        //Create window
        window = SDL_CreateWindow( progName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == nullptr ) {
            std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
            success = false;
        } else {
            //Get window surface
            screenSurface = SDL_GetWindowSurface( window );
        }
    }

    // Init SDLImage (not needed?)
    //IMG_Init(IMG_INIT_JPG);
    //IMG_Init(IMG_INIT_PNG);

    return success;
}

void cleanup() {
    //Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL_Image
    IMG_Quit();

    //Quit SDL subsystems
    SDL_Quit();
}

bool loadMedia() {

    bool success = true;

    // File to load
    std::string filename = "../../res/hello_world.bmp";
    //std::string filename = "../../res/image.jpg";
    //std::string filename = "../../res/image.gif";
    //std::string filename = "../../res/image.png";

    // Load image image
    image = IMG_Load(filename.c_str());

    if (image == nullptr) {
        std::cout << "Error loading file " << filename << " SDL_Error: " << SDL_GetError() << std::endl;
        success = false;
    }

    return success;
}

int main( int argc, char* args[] ) {

    if (!init()) {
        std::cout << "Failed to initialize\n" << std::endl;
    } else {

        if (!loadMedia()) {
            std::cout << "Error loading Media\n" << std::endl;
        } else {

            // Load image to surface
            SDL_BlitSurface(image, nullptr, screenSurface, nullptr);

            //Update the surface
            SDL_UpdateWindowSurface( window );

            // does not work on osx?? Use window-close event
            //SDL_Delay(5000);

            // wait for a window-close event
            bool isquit = false;
            SDL_Event event;
            while (!isquit) {
                if (SDL_PollEvent( & event)) {
                    if (event.type == SDL_QUIT) {
                        isquit = true;
                    }
                }
            }

        }

    }

    // Clean things up
    cleanup();

    return 0;
}