#version 440

in vec3 in_position;
in vec3 in_color;

uniform mat4 mvp;

out vec4 out_color;
out vec3 out_position;

void main() {

    out_color = vec4(in_color, 1.0);
    out_position = in_position;

    gl_Position = mvp * vec4(in_position, 1.0);

}
