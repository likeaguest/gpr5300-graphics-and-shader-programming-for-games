/**
 * 02_SDLevents - SDL Events
 */

#include <iostream>
#include <SDL2/SDL.h>

std::string progName = "02_SDLevents";

// Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// Window pointer (handler)
SDL_Window* window = nullptr;

// Window surface pointer
SDL_Surface* screenSurface = nullptr;


bool init() {
    // Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        success = false;
    } else {

        //Create window
        window = SDL_CreateWindow( progName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == nullptr ) {
            std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
            success = false;
        } else {
            //Get window surface
            screenSurface = SDL_GetWindowSurface( window );
        }
    }

    return success;
}

void cleanup() {
    //Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    SDL_Quit();
}

void runLoop() {
    bool loop = true;

    // infinite game loop
    while (loop) {
        // Event handler
        SDL_Event event;

        // Poll sdl events
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT)
                loop = false;
            if (event.type == SDL_KEYDOWN) {
                // Switch pressed keys and change bg-color
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        loop = false;
                        break;
                    case SDLK_r:
                        SDL_FillRect( screenSurface, nullptr, SDL_MapRGB( screenSurface->format, 0xFF, 0x00, 0x00 ) );
                        break;
                    case SDLK_g:
                        SDL_FillRect( screenSurface, nullptr, SDL_MapRGB( screenSurface->format, 0x00, 0xFF, 0x00 ) );
                        break;
                    case SDLK_b:
                        SDL_FillRect( screenSurface, nullptr, SDL_MapRGB( screenSurface->format, 0x00, 0x00, 0xFF ) );
                        break;
                    default:
                        break;
                }
                // Update surface
                SDL_UpdateWindowSurface( window );
            }
        }

    }

}


int main( int argc, char* args[] ) {

    if (!init()) {
        std::cout << "Failed to initialize\n" << std::endl;
    } else {

        //Fill the surface black
        SDL_FillRect( screenSurface, nullptr, SDL_MapRGB( screenSurface->format, 0x00, 0x00, 0x00 ) );

        //Update the surface (twice)
        SDL_UpdateWindowSurface( window );
        SDL_UpdateWindowSurface( window );

        runLoop();

    }

    // Clean things up
    cleanup();

    return 0;
}