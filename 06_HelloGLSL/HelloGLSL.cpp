#include "HelloGLSL.h"

#include "Camera.h"

#include "Shader.h"

std::string progName = "06_HelloGLSL";

// Our SDL_Window ( just like with SDL2 without OpenGL)
SDL_Window *mainWindow;

// Our opengl context handle
SDL_GLContext mainContext;

// SDL-Timer callback pointer
SDL_TimerID timerID;

// Frame Counter
int frameCount = 0;

// Avg FPS
float avgFps = 0;

int screenWidth = 512;
int screenHeight = 512;


//-----------------------------------------------------------------

GLuint g_ShaderProgram = 0;

GLint g_uniformMVP = -1;

GLuint g_vaoCube = 0;

//-----------------------------------------------------------------

Camera g_Camera;

glm::vec3 g_InitialCameraPosition = glm::vec3(0,0,7);

glm::quat g_InitialCameraRotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

glm::quat g_Rotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

int g_W, g_A, g_S, g_D;
int g_M1, g_M2;
int g_Wheel;

float g_MouseWheelSpeed = 1.0f;

bool g_RenderLoopRunning = true;

//-----------------------------------------------------------------

struct VertexXYZColor {
    glm::vec3 m_Pos;
    glm::vec3 m_Color;
};

VertexXYZColor g_Vertices[8] = {
        {glm::vec3( 1, 1, 1), glm::vec3(1,1,1)},
        {glm::vec3(-1, 1, 1), glm::vec3(0,1,1)},
        {glm::vec3(-1,-1, 1), glm::vec3(0,0,1)},
        {glm::vec3( 1,-1, 1), glm::vec3(1,0,1)},
        {glm::vec3( 1,-1,-1), glm::vec3(1,0,0)},
        {glm::vec3(-1,-1,-1), glm::vec3(0,0,0)},
        {glm::vec3(-1, 1,-1), glm::vec3(0,1,0)},
        {glm::vec3( 1, 1,-1), glm::vec3(1,1,0)}
};


GLuint g_Indices[36] = {
    0,1,2,2,3,0, // front face
    7,4,5,5,6,7, // back face
    6,5,2,2,1,6, // left face
    7,0,3,3,4,7, // right face
    7,6,1,1,0,7, // top face
    3,2,5,5,4,3  // bottom face
};


#define BUFFER_OFFSET(offset) ((void*)(offset))
#define MEMBER_OFFSET(s,m) ((char*)NULL + (offsetof(s,m)))

//-----------------------------------------------------------------


void Cleanup();


void PrintOpenGLVersion() {
    std::cout << "-------------------------------------------------------\n";
    std::cout << "INFO: OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Shader: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "-------------------------------------------------------\n";
}

void InitGLEW() {

    glewExperimental = GL_TRUE;

    if (glewInit() != GLEW_OK) {
        std::cerr << "Threr was a Problem initalizing GLEW\n";
        exit(1);
    }

    if (!GLEW_VERSION_4_4) {
        std::cerr << "OpenGL 4.4 required version support not present\n";
        exit(1);
    }

    std::cout << "Init GLEW done.\n";

}

void InitSDL() {
    // init SDL video system
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Failed to init SDL\n";
        exit(1);
    }

    // create window
    mainWindow = SDL_CreateWindow(
                progName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (!mainWindow) {
        std::cerr << "Unable to create window\n";
        //TODO: check SDL error
        exit(1);
    }

    // Set OpenGL context profile
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // set OpenGL version (version 3.3)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);

    // set double buffering
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //-------------------------------------------

    // create context
    mainContext = SDL_GL_CreateContext(mainWindow);

    if (!mainContext) {
        std::cerr << "Unable to create context\n";
        //TODO: check SDL error
        Cleanup();
        exit(1);
    }

    // turn (on/off) V-Sync
    SDL_GL_SetSwapInterval(0);

    std::cout << "Init SDL done.\n";

}

void InitGL() {
    // nothing to do here (just for now)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    std::cout << "Init GL done.\n";

}


void InitShader() {

    GLuint vertexShader = Shader::LoadShader(GL_VERTEX_SHADER, "../../res/shader/simpleShader.vert");
    GLuint fragmentShader = Shader::LoadShader(GL_FRAGMENT_SHADER, "../../res/shader/simpleShader.frag");

    std::vector<GLuint> shaders;
    shaders.push_back(vertexShader);
    shaders.push_back(fragmentShader);

    g_ShaderProgram = Shader::CreateShaderProgram(shaders);
    assert(g_ShaderProgram != 0);

    std::cout << "Init Shader done.\n";

}

void InitViewport() {
    g_Camera.SetPosition(g_InitialCameraPosition);
    g_Camera.SetRotation(g_InitialCameraRotation);

    g_Camera.SetViewport(0,0,screenWidth, screenHeight);
    g_Camera.SetProjectionRH(60.0f, screenWidth / (float) screenHeight, 0.1f, 100.0f);

    std::cout << "Init Viewport done.\n";

}

void InitVAO() {

    GLuint positionAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_position");
    GLuint colorAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_color");

    g_uniformMVP = glGetUniformLocation(g_ShaderProgram, "mvp");

    // Create a VAO for the cube
    glGenVertexArrays(1, &g_vaoCube);
    glBindVertexArray(g_vaoCube);

    // Create buffer
    GLuint vertexBuffer, indexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &indexBuffer);

    // Bind buffer for vertices
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_Vertices), g_Vertices, GL_STATIC_DRAW);

    // Bind buffer for indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(g_Indices), g_Indices, GL_STATIC_DRAW);

    // Vertices
    glVertexAttribPointer(positionAttribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor, m_Pos));
    glEnableVertexAttribArray(positionAttribID);

    // Colors
    glVertexAttribPointer(colorAttribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor, m_Color));
    glEnableVertexAttribArray(colorAttribID);


    // Clean up
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(positionAttribID);
    glDisableVertexAttribArray(colorAttribID);

    // cout
    std::cout << "VAO + VBO initialized.\n";
}

void Cleanup() {
    SDL_GL_DeleteContext(mainContext);

    SDL_DestroyWindow(mainWindow);

    SDL_Quit();
}

void DrawScene(float tpf) {

    // clear color buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glBindVertexArray( g_vaoCube );
    glUseProgram( g_ShaderProgram );

    glm::mat4 mvp = g_Camera.GetProjectionMatrix() * g_Camera.GetViewMatrix() * glm::toMat4(g_Rotation);
    glUniformMatrix4fv( g_uniformMVP, 1, GL_FALSE, glm::value_ptr(mvp));

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, BUFFER_OFFSET(0));

    // clean up
    glUseProgram(0);
    glBindVertexArray(0);

    // swap back and front buffer (show frame)
    SDL_GL_SwapWindow(mainWindow);

    // inc FrameCount
    frameCount++;
}

void MoveObjects(float tpf) {

    g_Camera.Translate(glm::vec3((g_A - g_D), (g_S - g_W), 0) * 0.001f);

    if (!g_M1) {
        glm::quat rotX = glm::angleAxis<float>(0.001f * tpf, glm::vec3(1,0,0));
        glm::quat rotY = glm::angleAxis<float>(0.001f * tpf, glm::vec3(0,1,0));
        g_Rotation = g_Rotation * rotX * rotY;
    }
}

Uint32 CalcFPS(Uint32 interval, void *param) {

    // Calc avg fps
    avgFps = frameCount / (interval / 1000.0f);

    // reset frame count
    frameCount = 0;

    // Set window title with fps
    std::stringstream winTitle;

    winTitle << progName << " @ " << std::fixed << std::setprecision(1) << avgFps << " fps";

    SDL_SetWindowTitle(mainWindow, winTitle.str().c_str());

    timerID = SDL_AddTimer(interval, CalcFPS, param);

    return 0;
}

void SDLKeyboardHandler(SDL_Keysym key, int down) {
    switch (key.sym) {
        case SDLK_w:
            g_W = down;
            break;
        case SDLK_a:
            g_A = down;
            break;
        case SDLK_s:
            g_S = down;
            break;
        case SDLK_d:
            g_D = down;
            break;
        case SDLK_r:
            g_Camera.SetPosition(g_InitialCameraPosition);
            g_Camera.SetRotation(g_InitialCameraRotation);
            g_Rotation = QUAT_IDENTITY;
            break;
        case SDLK_ESCAPE:
            g_RenderLoopRunning = false;
            break;
        default:
            break;
    }
}

void MouseWheelHandler(SDL_MouseWheelEvent event) {
    g_Camera.Translate(glm::vec3(0, 0, -event.y * g_MouseWheelSpeed));
}

void MouseMoveHandler(SDL_MouseMotionEvent event) {
    if (g_M2) {
        float speed = 0.01f;
        g_Camera.Translate(glm::vec3(-event.xrel, event.yrel, 0) * speed);
    }

    if (g_M1) {
        // MAJOR TODO: fix this
        glm::vec3 axis_x = glm::vec3(1, 0, 0) * glm::toMat3(g_Rotation);
        glm::vec3 axis_y = glm::vec3(0, 1, 0) * glm::toMat3(g_Rotation);

        glm::quat rotX = glm::angleAxis<float>(glm::radians(0.2f * event.yrel), glm::normalize(axis_x));
        glm::quat rotY = glm::angleAxis<float>(glm::radians(0.2f * event.xrel), glm::normalize(axis_y));

        g_Rotation = g_Rotation * rotX * rotY;
    }
}

void Render() {

    timerID = SDL_AddTimer(3 * 1000 , CalcFPS, nullptr);

    Uint64 now = SDL_GetPerformanceCounter();
    Uint64 last = 0;
    float tpf = 0;

    while (g_RenderLoopRunning) {

        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_WINDOWEVENT) {
                if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                    int w;
                    int h;
                    SDL_GetWindowSize(mainWindow, &w, &h);
                    g_Camera.SetViewport(0,0, w, h);
                    std::cout << "It happened!";
                }
            }

            if (event.type == SDL_QUIT) g_RenderLoopRunning = false;

            if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 1);
            }

            if (event.type == SDL_KEYUP && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 0);
            }

            if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (event.button.button == SDL_BUTTON_LEFT) {
                    g_M1 = 1;
                }
                if (event.button.button == SDL_BUTTON_RIGHT) {
                    g_M2 = 1;
                }
            }

            if (event.type == SDL_MOUSEBUTTONUP) {
                if (event.button.button == SDL_BUTTON_LEFT) {
                    g_M1 = 0;
                }
                if (event.button.button == SDL_BUTTON_RIGHT) {
                    g_M2 = 0;
                }
            }

            if (event.type == SDL_MOUSEMOTION) {
                MouseMoveHandler(event.motion);
            }

            if (event.type == SDL_MOUSEWHEEL) {
                MouseWheelHandler(event.wheel);
            }
        }

        // Move objects
        MoveObjects(tpf);

        // draw the scene
        DrawScene(tpf);

        // calc tpf
        last = now;
        now = SDL_GetPerformanceCounter();
        tpf = ((now - last) * 1000.0 /(float) SDL_GetPerformanceFrequency());
    }
}
int main(int argc, char *argv[]) {

    InitSDL();

    InitGLEW();

    InitGL();

    InitShader();

    InitVAO();

    InitViewport();

    PrintOpenGLVersion();

    std::cout << "Rendering...\n";

    Render();

    std::cout << "Rendering done, bye.\n";

    Cleanup();

    return 0;
}

