#version 440

in vec3 in_position;
in vec3 in_color;

uniform mat4 mvp;

out vec4 out_color;

void main() {

    out_color = vec4(in_color, 1.0);

    gl_Position = mvp * vec4(in_position, 1.0);

}
