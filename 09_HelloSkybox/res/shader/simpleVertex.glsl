#version 440

in vec3 in_position;
in vec3 in_color;

// Model, View, Projection matrix.
uniform mat4 MVP;

out vec4 v2f_color;
out vec3 v2f_position;

void main() {

    // Just pass the color through directly.
    v2f_color = vec4(in_color, 1);

    // Just pass the vertices through directly.
    v2f_position = in_position;

    gl_Position = MVP * vec4(in_position, 1);


    //vec4 pos = MVP * vec4(in_position, 1);
    //gl_Position = pos.xyww;


}