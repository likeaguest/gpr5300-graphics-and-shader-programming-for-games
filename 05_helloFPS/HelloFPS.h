#ifndef _HELLOFPS_H_
#define _HELLOFPS_H_

#include <iostream>
#include <iomanip>
#include <sstream>


//try static linking glew(opengl)
//#define GLEW_STATIC 1


// OpenGL / GLEW Headers
#include <GL/glew.h>


#ifdef __WIN32__
#include <GL/wglew.h>
#endif



// SDL2 Headers
#include <SDL2/SDL.h>


#endif //_HELLOFPS_H_