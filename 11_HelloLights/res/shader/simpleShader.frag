#version 440

in vec4 out_color;
in vec3 out_position;

uniform samplerCube skybox;

out vec4 fragColor;

void main() {
    vec4 texel = texture(skybox, out_position);
    fragColor = texel;
}
