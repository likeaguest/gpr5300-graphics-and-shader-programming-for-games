#version 440

in vec2 v2f_texcoord;
in vec4 v2f_positionW;
in vec4 v2f_normalW;

uniform sampler2D surface;
uniform sampler2D specularMap;

uniform vec4 u_eyePosW;
uniform vec4 u_lightPosW;
uniform vec4 u_lightColor;

uniform vec4 u_materialEmissive;
uniform vec4 u_materialDiffuse;
uniform vec4 u_materialSpecular;

uniform float u_materialShininess;

uniform vec4 u_ambient;

out vec4 fragColor;

void main() {
    // Compute ambient
    vec4 ambient = u_ambient;

    // Compute emissive
    vec4 emissive = u_materialEmissive;

    // Compute diffuse
    vec4 n = normalize(v2f_normalW);
    vec4 l = normalize(u_lightPosW - v2f_positionW);
    float ndotl = max(dot(n, l), 0);
    vec4 diffuse = ndotl * u_lightColor * u_materialDiffuse;

    // Compute specular
    vec4 v = normalize(u_eyePosW - v2f_positionW);
    vec4 r = reflect(-l, n);
    float rdotv = max(dot(r, v), 0);

    vec4 specular = pow(rdotv, u_materialShininess) * u_lightColor * u_materialSpecular;

    // Compute surface color
    vec4 surfaceColor = texture(surface, v2f_texcoord);
    vec4 specMap = texture(specularMap, v2f_texcoord);

    // Compute light term
    vec4 lightTermWater = ambient + emissive + diffuse + specular;
    vec4 lightTermTerrain = ambient + emissive + diffuse;

    if (specMap.r > 0.5f) {
        fragColor = surfaceColor * lightTermTerrain;
    } else {
        fragColor = surfaceColor * lightTermWater;
    }
}
