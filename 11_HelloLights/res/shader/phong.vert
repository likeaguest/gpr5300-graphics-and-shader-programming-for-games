#version 440

in vec3 in_position;
in vec3 in_normal;
in vec2 in_texcoord;

uniform mat4 u_mvp;
uniform mat4 u_modelMatrix;

out vec2 v2f_texcoord;
out vec4 v2f_positionW;
out vec4 v2f_normalW;

void main() {
    v2f_texcoord = in_texcoord;
    gl_Position = u_mvp * vec4(in_position, 1.0);

    v2f_positionW = u_modelMatrix * vec4(in_position, 1.0);
    v2f_normalW = u_modelMatrix * vec4(in_normal, 0.0);
}
