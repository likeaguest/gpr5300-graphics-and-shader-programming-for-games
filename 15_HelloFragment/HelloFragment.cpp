#include "HelloFragment.h"

#include "Camera.h"

#include "Shader.h"

#include "Texture.h"

std::string progName = "15_HelloTFragment";

// Our SDL_Window ( just like with SDL2 wihout OpenGL)
SDL_Window *mainWindow;

// Our opengl context handle
SDL_GLContext mainContext;

// SDL-Timer callback pointer
SDL_TimerID timerID;

// Frame Counter
int frameCount = 0;

// Avg FPS
float avgFps = 0;

int screenWidth = 512;
int screenHeight = 512;


//-----------------------------------------------------------------

GLuint g_ShaderProgram = 0;

//-----------------------------------------------------------------

GLint g_uniformResolution;
GLint g_uniformMouse;
GLint g_uniformTime;


GLint g_uniformMVP;




//-----------------------------------------------------------------


GLuint g_vaoCube = 0;

GLuint g_Texture;

//-----------------------------------------------------------------

Camera g_Camera;

glm::vec3 g_InitialCameraPosition = glm::vec3(0,0,1);

//glm::quat g_InitialCameraRotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

glm::quat g_Rotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

bool stopRenderLoop = false;

//-----------------------------------------------------------------

int g_W, g_A, g_S, g_D;

int g_M1, g_M2;

glm::vec2 mousePosition(0,0);

float renderTime = 0.0;

//-----------------------------------------------------------------

struct VertexXYZColor {
    glm::vec3 m_Pos;
    glm::vec3 m_Color;
    glm::vec2 m_UV;
};

VertexXYZColor g_Vertices[4] = {
        {glm::vec3( -1, -1, 0), glm::vec3(1,0,0), glm::vec2(0,1)},
        {glm::vec3(  1, -1, 0), glm::vec3(0,1,0), glm::vec2(1,1)},
        {glm::vec3( -1,  1, 0), glm::vec3(0,0,1), glm::vec2(0,0)},
        {glm::vec3(  1,  1, 0), glm::vec3(1,1,1), glm::vec2(1,0)},
};


GLuint g_Indices[6] = {
    0,1,2,
    2,1,3
};




#define BUFFER_OFFSET(offset) ((void*)(offset))
#define MEMBER_OFFSET(s,m) ((char*)NULL + (offsetof(s,m)))

//-----------------------------------------------------------------


void Cleanup();


void PrintOpenGLVersion() {
    std::cout << "-------------------------------------------------------\n";
    std::cout << "INFO: OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Shader: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "-------------------------------------------------------\n";
}

void InitGLEW() {

    glewExperimental = GL_TRUE;

    if (glewInit() != GLEW_OK) {
        std::cerr << "Threr was a Problem initalizing GLEW\n";
        exit(1);
    }

    if (!GLEW_VERSION_4_4) {
        std::cerr << "OpenGL 4.4 required version support not present\n";
        exit(1);
    }

    std::cout << "Init GLEW done.\n";

}

void InitSDL() {
    // init SDL video system
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Faild to init SDL\n";
        exit(1);
    }

    // create window
    mainWindow = SDL_CreateWindow(
                progName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL);

    if (!mainWindow) {
        std::cerr << "Unable to create window\n";
        //TODO: check SDL error
        exit(1);
    }

    // Set OpenGL context profile
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // set OpenGL version (version 3.3)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);

    // set double buffering
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //-------------------------------------------

    // create context
    mainContext = SDL_GL_CreateContext(mainWindow);

    if (!mainContext) {
        std::cerr << "Unable to create context\n";
        //TODO: check SDL error
        Cleanup();
        exit(1);
    }

    // turn (on/off) V-Sync
    SDL_GL_SetSwapInterval(0);

    std::cout << "Init SDL done.\n";

}

void InitGL() {
    // nothing to do here (just for now)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    std::cout << "Init GL done.\n";

}


void InitShader() {

    GLuint vertexShader = Shader::LoadShader(GL_VERTEX_SHADER, "../../res/shader/pixelShader.vert");

    //GLuint fragmentShader = Shader::LoadShader(GL_FRAGMENT_SHADER, "../../res/shader/pixelShader.frag");
    //GLuint fragmentShader = Shader::LoadShader(GL_FRAGMENT_SHADER, "../../res/shader/colorShader.frag");
    GLuint fragmentShader = Shader::LoadShader(GL_FRAGMENT_SHADER, "../../res/shader/gridShader.frag");

    std::vector<GLuint> shaders;
    shaders.push_back(vertexShader);
    shaders.push_back(fragmentShader);

    g_ShaderProgram = Shader::CreateShaderProgram(shaders);
    assert(g_ShaderProgram != 0);

    std::cout << "Init Shader done.\n";

}

void InitViewport() {

    g_Camera.SetPosition(g_InitialCameraPosition);
    //g_Camera.SetRotation(g_InitialCameraRotation);

    g_Camera.SetViewport(0,0,screenWidth, screenHeight);
    g_Camera.SetProjectionRH(60.0f, screenWidth / (float) screenHeight, 0.1f, 100.0f);

    std::cout << "Init Viewport done.\n";

}

void InitVAO() {

    GLuint positionAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_position");
    GLuint colorAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_color");
    GLuint textureAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_uv");

    g_uniformMVP = glGetUniformLocation(g_ShaderProgram, "mvp");

    g_uniformResolution = glGetUniformLocation(g_ShaderProgram, "u_resolution");
    g_uniformMouse = glGetUniformLocation(g_ShaderProgram, "u_mouse");
    g_uniformTime = glGetUniformLocation(g_ShaderProgram, "u_time");


    // Create a VAO for the cube
    glGenVertexArrays(1, &g_vaoCube);
    glBindVertexArray(g_vaoCube);

    // Create buffer
    GLuint vertexBuffer, indexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &indexBuffer);

    // Bind buffer for vertices
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_Vertices), g_Vertices, GL_STATIC_DRAW);

    // Bind buffer for indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(g_Indices), g_Indices, GL_STATIC_DRAW);

    // Vertices
    glVertexAttribPointer(positionAttribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor,m_Pos));
    glEnableVertexAttribArray(positionAttribID);

    // Colors
    glVertexAttribPointer(colorAttribID, 3, GL_FLOAT, GL_FALSE, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor,m_Color));
    glEnableVertexAttribArray(colorAttribID);

    // UV
    glVertexAttribPointer(textureAttribID, 2, GL_FLOAT, GL_FALSE, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor,m_UV));
    glEnableVertexAttribArray(textureAttribID);

    // Clean up
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(positionAttribID);
    glDisableVertexAttribArray(colorAttribID);
    glDisableVertexAttribArray(textureAttribID);

    // cout
    std::cout << "VAO + VBO initialized.\n";

}


void InitTexture() {

    g_Texture = Texture::LoadTexture("../../res/textures/sponge_front.png");

    // cout
    std::cout << "Textures initialized.\n";

}



void Cleanup() {
    SDL_GL_DeleteContext(mainContext);

    SDL_DestroyWindow(mainWindow);

    SDL_Quit();
}

void DrawScene(float tpf) {

    // clear color buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glBindVertexArray( g_vaoCube );
    glUseProgram( g_ShaderProgram );

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_Texture);

    //---------------------------------------------------

    glm::vec2 resolution = glm::vec2(screenWidth, screenHeight);
    glUniform2fv( g_uniformResolution, 1, glm::value_ptr(resolution));


    glUniform2fv( g_uniformMouse, 1, glm::value_ptr(mousePosition));


    glUniform1f( g_uniformTime, renderTime);



    //---------------------------------------------------

    glm::mat4 projection_matrix = g_Camera.GetProjectionMatrix();
    glm::mat4 view_matrix = g_Camera.GetViewMatrix();

    //------------------------

    glm::mat4 model_translation = glm::translate(glm::vec3(0, 2, 0));
    glm::mat4 model_scale = glm::scale(glm::vec3(1, 1.5, 1));
    glm::mat4 model_rotation = glm::toMat4(g_Rotation);



    glm::mat4 model_matrix = model_translation * model_rotation * model_scale;
    model_matrix = MAT4_IDENTITY;

    //------------------------


    glm::mat4 mvp = projection_matrix * view_matrix * model_matrix;

    //-----------------------------------------------------------


    glUniformMatrix4fv( g_uniformMVP, 1, GL_FALSE, glm::value_ptr(mvp));

    // TODO: add indeces size
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, BUFFER_OFFSET(0));

    // clean up
    glUseProgram(0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    // swap back and front buffer (show frame)
    SDL_GL_SwapWindow(mainWindow);

    // inc FrameCount
    frameCount++;
}

void MoveObjects(float tpf) {

    float speed = 0.005f * tpf;

    //g_Camera.Translate(glm::vec3((g_A - g_D), (g_S - g_W),0) * speed);

/*
    if (!g_M1) {
        glm::quat rotX = glm::angleAxis<float>(0.001f * tpf, glm::vec3(1,0,0));
        glm::quat rotY = glm::angleAxis<float>(0.001f * tpf, glm::vec3(0,1,0));

        g_Rotation = g_Rotation * rotX * rotY;
    }
*/


}

Uint32 CalcFPS(Uint32 interval, void *param) {

    // Calc avg fps
    avgFps = frameCount / (interval / 1000.0f);

    // reset frame count
    frameCount = 0;

    // Set window title with fps
    std::stringstream winTitle;

    winTitle << progName << " @ " << std::fixed << std::setprecision(1) << avgFps << " fps";

    SDL_SetWindowTitle(mainWindow, winTitle.str().c_str());

    timerID = SDL_AddTimer(interval, CalcFPS, param);

    return 0;

}

void SDLKeyboardHandler(SDL_Keysym key, int down) {
    switch(key.sym) {
        case SDLK_w:
            g_W = down;
            break;
        case SDLK_a:
            g_A = down;
            break;
        case SDLK_s:
            g_S = down;
            break;
        case SDLK_d:
            g_D = down;
            break;
        case SDLK_r:
            g_Camera.SetPosition(g_InitialCameraPosition);
           // g_Camera.SetRotation(g_InitialCameraRotation);
            g_Rotation = QUAT_IDENTITY;
            break;
        case SDLK_ESCAPE:
            stopRenderLoop = true;
            break;
        default:
            break;
    }
}

void MouseMoveHandler(SDL_MouseMotionEvent event) {

    mousePosition = glm::vec2(event.x, event.y);

    if (g_M2) {

        float speed = 0.01f;

        g_Camera.Translate(glm::vec3(-event.xrel, event.yrel, 0) * speed);
    }

    if (g_M1) {

        glm::vec3 axis_x = glm::normalize(glm::vec3(1,0,0) * glm::toMat3(g_Rotation));
        glm::vec3 axis_y = glm::normalize(glm::vec3(0,1,0) * glm::toMat3(g_Rotation));

        glm::quat rot_x = glm::angleAxis<float>(0.01 * event.yrel, axis_x);
        glm::quat rot_y = glm::angleAxis<float>(0.01 * event.xrel, axis_y);

        g_Rotation = g_Rotation * rot_x * rot_y;
    }


}

void MouseWheelHandler(SDL_MouseWheelEvent event) {

    float speed = 0.5f;

    g_Camera.Translate(glm::vec3(0,0,-event.y * speed));

}



void Render() {

    timerID = SDL_AddTimer(3 * 1000 , CalcFPS, nullptr);

    Uint64 now = SDL_GetPerformanceCounter();
    Uint64 last = 0;
    float tpf = 0;

     while (!stopRenderLoop) {

        SDL_Event event;
        while (SDL_PollEvent(&event)) {

            if (event.type == SDL_QUIT) stopRenderLoop = true;

            if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 1);
            }

            if (event.type == SDL_KEYUP && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 0);
            }

            if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (event.button.button == SDL_BUTTON_LEFT) g_M1 = 1;
                if (event.button.button == SDL_BUTTON_RIGHT) g_M2 = 1;
            }

            if (event.type == SDL_MOUSEBUTTONUP) {
                if (event.button.button == SDL_BUTTON_LEFT) g_M1 = 0;
                if (event.button.button == SDL_BUTTON_RIGHT) g_M2 = 0;
            }

            if (event.type == SDL_MOUSEMOTION)
                MouseMoveHandler(event.motion);

            if (event.type == SDL_MOUSEWHEEL)
                MouseWheelHandler(event.wheel);

        }

        // Move objects
        MoveObjects(tpf);

        // draw the scene
        DrawScene(tpf);

        // calc tpf
        last = now;
        now = SDL_GetPerformanceCounter();
        tpf = ((now - last) * 1000.0 /(float) SDL_GetPerformanceFrequency());

        // store time
        renderTime = SDL_GetTicks() / 1000.0f;

     }
}

int main(int argc, char *argv[]) {

    InitSDL();

    InitGLEW();

    InitGL();

    InitShader();

    InitVAO();

    InitTexture();

    InitViewport();

    PrintOpenGLVersion();

    std::cout << "Rendering...\n";

    Render();

    std::cout << "Rendering done, bye.\n";

    Cleanup();

    return 0;
}

