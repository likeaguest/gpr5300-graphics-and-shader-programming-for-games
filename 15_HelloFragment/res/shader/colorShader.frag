#version 440

in vec4 out_color;
in vec2 out_uv;

uniform vec2 u_resolution;  // Canvas size (width,height)
uniform vec2 u_mouse;       // mouse position in screen pixels
uniform float u_time;       // Time in seconds since load

uniform sampler2D sampler;

out vec4 fragColor;


#define PI 3.14159265359
#define TWO_PI 6.28318530718


//  Function from Iñigo Quiles
//  https://www.shadertoy.com/view/MsS3Wc
vec3 hsb2rgb( in vec3 c ) {
    vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0), 6.0)-3.0)-1.0, 0.0, 1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb);
    return c.z * mix(vec3(1.0), rgb, c.y);
}



void main() {

    vec2 st = gl_FragCoord.xy/u_resolution;

    vec3 color = vec3(0.0);

    //float x = st.x + u_time;
    //color = hsb2rgb(vec3(x,1.0,st.y));


    // Use polar coordinates instead of cartesian
    vec2 toCenter = vec2(0.5)-st;
    float angle = atan(toCenter.y, toCenter.x) * u_time;
    float radius = length(toCenter)*2.0;

    // Map the angle (-PI to PI) to the Hue (from 0 to 1)
    // and the Saturation to the radius
    color = hsb2rgb(vec3((angle/TWO_PI)+0.5,radius,1.0));

    fragColor = vec4(color, 1.0);


}
