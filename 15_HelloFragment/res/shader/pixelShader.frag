#version 440

in vec4 out_color;
in vec2 out_uv;

uniform vec2 u_resolution;  // Canvas size (width,height)
uniform vec2 u_mouse;       // mouse position in screen pixels
uniform float u_time;       // Time in seconds since load

uniform sampler2D sampler;

out vec4 fragColor;


#define PI 3.14159265359


// Plot a line on Y using a value between 0.0-1.0
float plot(vec2 st, float pct){
    return  smoothstep( pct-0.02, pct, st.y) - smoothstep( pct, pct+0.02, st.y);
}

void main() {


    vec2 uv = out_uv;

    // Normalized pixel coordinates (from 0 to 1)
    vec2 st = gl_FragCoord.xy / u_resolution;
    st=uv;


    vec2 mouse = u_mouse / u_resolution;
    mouse.y = 1.0 - mouse.y;

    // Time varying pixel color
    vec3 lsdColor = 0.5 + 0.5 * cos(u_time + st.xyx + vec3(0,2,4));

    // Output to screen
    //fragColor = vec4(lsdColor, 1.0) * abs(sin(u_time));

    // U,V as red and green
    //fragColor = vec4(mouse.x, mouse.y, 0.0, 1.0);

    //-------------------------------------------------
    // f(x) = x; y=x
    //float y = st.x;

    // y = x^2
    //float y = pow(st.x, 4.0);


    //float y = step(0.5, st.x);


    //float y = smoothstep(0.2, 0.7, st.x);

    //float y = clamp(st.x, 0.2, 0.7);

    //float y = sin(st.x * 2.0 * PI) * 0.5 + 0.5;

    float x = st.x * 2.0;

    float y = 1.0 - pow(abs(sin(PI * x / 2.0)), 3.0);


    vec3 color = vec3(y);

    // Plot a line
    float pct = plot(st, y);
    color = (1.0-pct) * color + pct * vec3(0.0,1.0,0.0);

    fragColor = vec4(color, 1.0);


    /*
    float t = u_time;
    vec2 r = u_resolution;


    vec3 c;
    float l,z=t;
    for(int i=0;i<3;i++) {
        vec2 uv,p=gl_FragCoord.xy/r;
        uv=p;
        p-=.5;
        p.x*=r.x/r.y;
        z+=.07;
        l=length(p);
        uv+=p/l*(sin(z)+1.)*abs(sin(l*9.-z*2.));
        c[i]=.01/length(abs(mod(uv,1.)-.5));
    }
    fragColor=vec4(c/l,t);
    */

}
