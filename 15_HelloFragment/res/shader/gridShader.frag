#version 440

in vec4 out_color;
in vec2 out_uv;

uniform vec2 u_resolution;  // Canvas size (width,height)
uniform vec2 u_mouse;       // mouse position in screen pixels
uniform float u_time;       // Time in seconds since load

uniform sampler2D sampler;

out vec4 fragColor;


#define PI 3.14159265358979323846


vec2 rotate2D(vec2 _st, float _angle){
    _st -= 0.5;
    _st =  mat2(cos(_angle),-sin(_angle),sin(_angle),cos(_angle)) * _st;
    _st += 0.5;
    return _st;
}

vec2 tile(vec2 _st, float _zoom){
    _st *= _zoom;
    return fract(_st);
}

float box(vec2 _st, vec2 _size, float _smoothEdges){
    _size = vec2(0.5)-_size*0.5;
    vec2 aa = vec2(_smoothEdges*0.5);
    vec2 uv = smoothstep(_size,_size+aa,_st);
    uv *= smoothstep(_size,_size+aa,vec2(1.0)-_st);
    return uv.x*uv.y;
}




void main() {

    vec2 st = gl_FragCoord.xy/u_resolution;

    vec3 color = vec3(0.0);

    vec3 lsdColor = 0.5 + 0.5 * cos(u_time + st.xyx + vec3(0,2,4));


    // add some magic waves
    vec2 stMagic = rotate2D(st, PI*1.0*u_time);
    //stMagic = st;
    float magicWave = stMagic.y*stMagic.x*cos(u_time)*2.0;

    // Divide the space in 4
    st = tile(st, 10.0 + magicWave);

    // Use a matrix to rotate the space 45 degrees
    st = rotate2D(st, sin(u_time * 0.5) * PI * 0.25 + (PI * 0.5));


    vec3 fake3dColor = 1.0 - vec3(length(st)) * 0.8;

    color = vec3(box(st,vec2(0.7),0.01)) * lsdColor * fake3dColor;

    fragColor = vec4(color, 1.0);


}
