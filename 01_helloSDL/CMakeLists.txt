# CMAKE required minimum version
cmake_minimum_required(VERSION 3.6)

# Project (binary) name
project(01_helloSDL)

# Set bin output
set(EXECUTABLE_OUTPUT_PATH "Debug")

########################################################################################
#    DETECT OSX
########################################################################################
IF (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    # Set a global flag for OSX
    set(MACOSX TRUE)
ENDIF()
########################################################################################


# Detect Operating-System
IF (WIN32)
    ####################################################################################
    #    WINDOWS (copy SDL2.dll to your binary folder ./bin/Debug/)
    ####################################################################################

    # Set include and library paths SDL
    set(SDL2_INCLUDE_DIR D:/Libraries/SDL2-2.0.9/x86_64-w64-mingw32/include)
    set(SDL2_LIBRARY_DIR D:/Libraries/SDL2-2.0.9/x86_64-w64-mingw32/lib)

    include_directories(${SDL2_INCLUDE_DIR})
    link_directories(${SDL2_LIBRARY_DIR})

    ####################################################################################

    # Additional Compiler Flags
    set(COMPILER_FLAGS "-lmingw32 -static-libstdc++ -static-libgcc")

    # Libraries to be linked
    set(LINK_LIBRARIES SDL2main SDL2)

ELSEIF(MACOSX)
    ####################################################################################
    #    OSX
    ####################################################################################

    # Add custom sdl2-cmake files to cmake module path
    #   (install sdl2-framework[s] & copy sdl2_cmake_scripts to your project-root (/bin/cmake/sdl2/FindSDL2***.cmake))
    list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/bin/cmake/sdl2)

    # run FindSDL2.cmake helper script
    find_package(SDL2 REQUIRED)

    ####################################################################################

    # Libraries to be linked
    set(LINK_LIBRARIES ${SDL2_LIBRARIES})


ELSE()
    ####################################################################################
    #    UNIX
    ####################################################################################

    # Libraries to be linked
    set(LINK_LIBRARIES SDL2)

ENDIF()

########################################################################################
#    4All
########################################################################################

# Set C++ version via compiler flag
set(GLOBAL_COMPILER_FLAGS "-std=c++11")

# Set All Compiler Flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GLOBAL_COMPILER_FLAGS} ${COMPILER_FLAGS}")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

# Set source files
set(SOURCE_FILES main.cpp)

# Set (add) executable name
add_executable(${PROJECT_NAME} ${SOURCE_FILES})

# Link (runtime) libraries
target_link_libraries(${PROJECT_NAME} ${LINK_LIBRARIES})
