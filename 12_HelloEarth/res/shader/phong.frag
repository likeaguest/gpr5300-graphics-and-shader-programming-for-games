#version 440

in vec2 v2f_texcoord;
in vec4 v2f_positionW;
in vec4 v2f_normalW;

uniform sampler2D dayMap;
uniform sampler2D nightMap;
uniform sampler2D cloudMap;

uniform sampler2D specularMap;

uniform vec4 u_eyePosW;
uniform vec4 u_lightPosW;
uniform vec4 u_lightColor;

uniform vec4 u_materialEmissive;
uniform vec4 u_materialDiffuse;
uniform vec4 u_materialSpecular;

uniform float u_materialShininess;

uniform vec4 u_ambient;

uniform float u_renderTime;


out vec4 fragColor;

void main() {

    // Compute Ambient term
    vec4 Ambient = u_ambient;

    // Compute Emissive term
    vec4 Emissive = u_materialEmissive;

    // Compute Diffuse term
    vec4 N = normalize(v2f_normalW);
    vec4 L = normalize(u_lightPosW - v2f_positionW);
    float NdotL = max(dot(N, L), 0);
    vec4 Diffuse = NdotL * u_lightColor * u_materialDiffuse;


    // Compute Specular term
    vec4 V = normalize(u_eyePosW - v2f_positionW);
    //TODO: add H vector
    vec4 R = reflect(-L, N);
    float RdotV = max(dot(R, V), 0);

    vec4 Specular = pow(RdotV, u_materialShininess) * u_lightColor * u_materialSpecular;

    //-----------------------------------------------------------------------------------------

    // Texture colors
    vec4 dayMapColor = texture(dayMap, v2f_texcoord);
    vec4 nightMapColor = texture(nightMap, v2f_texcoord) * 2.5f;
    vec4 cloundMapColor = texture(cloudMap, vec2(v2f_texcoord.x - u_renderTime, v2f_texcoord.y));
    vec4 specularMapColor = texture(specularMap, v2f_texcoord);


    vec4 lightTerm;

    // Calc whole light term
    if (specularMapColor.r > 0.5) {
        lightTerm = Ambient + Emissive + Diffuse + Specular;
    } else {
        lightTerm = Ambient + Emissive + Diffuse;
    }

    // mixture of day and night
    vec4 surface = mix(dayMapColor + cloundMapColor, nightMapColor, 1.0 - Diffuse.r);

    // output color
    fragColor = surface * lightTerm;




}
