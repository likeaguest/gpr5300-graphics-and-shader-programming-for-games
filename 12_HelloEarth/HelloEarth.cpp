#include "HelloEarth.h"

#include "Camera.h"

#include "Shader.h"

#include "Texture.h"

#include "Sphere.h"

std::string progName = "12_HelloEarth";

// Our SDL_Window ( just like with SDL2 wihout OpenGL)
SDL_Window *mainWindow;

// Our opengl context handle
SDL_GLContext mainContext;

// SDL-Timer callback pointer
SDL_TimerID timerID;

// Frame Counter
int frameCount = 0;

// Avg FPS
float avgFps = 0;

int screenWidth = 512;
int screenHeight = 512;


//-----------------------------------------------------------------

GLuint g_ShaderProgram = 0;

GLint g_uniformMVP = -1;
GLint g_uniformModelMatrix = -1;


GLuint g_vaoCube = 0;

GLuint g_TextureDayMap;
GLuint g_TextureNightMap;
GLuint g_TextureCloudMap;

GLuint g_TextureSpecularMap;




//-----------------------------------------------------------------

Sphere* g_sphere;

GLuint g_vaoSphere;

//-----------------------------------------------------------------

Camera g_Camera;

glm::vec3 g_InitialCameraPosition = glm::vec3(0,0,3);

//glm::quat g_InitialCameraRotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

glm::quat g_Rotation = QUAT_IDENTITY;

//-----------------------------------------------------------------

bool stopRenderLoop = false;

//-----------------------------------------------------------------

int g_W, g_A, g_S, g_D;

int g_M1, g_M2, g_M3;


//-----------------------------------------------------------------


glm::quat g_SunRotation = QUAT_IDENTITY;
glm::vec3 g_SunPosition(10,10,10);

const glm::vec4 g_lightColor(1,1,1,1);


const glm::vec4 g_ambientColor(0.2, 0.2, 0.2, 1);

const glm::vec4 g_emissiveColor(0.35, 0.35, 0.35, 1);

const glm::vec4 g_diffuseColor(1,1,1,1);

const glm::vec4 g_specularColor(3,3,3,1);

const float g_shininess = 50.0f;

//----------------------------------------------------------------


GLint g_uniformEyePosW;
GLint g_uniformLightPosW;

GLint g_uniformLightColor;

GLint g_uniformAmbient;
GLint g_uniformMaterialEmissive;
GLint g_uniformMaterialDiffuse;
GLint g_uniformMaterialSpecular;
GLint g_uniformMaterialShininess;


GLint g_uniformDayMap;
GLint g_uniformNightMap;
GLint g_uniformCloudMap;

GLint g_uniformSpecularMap;


GLint g_uniformRenderTime;




//-----------------------------------------------------------------
void Cleanup();


void PrintOpenGLVersion() {
    std::cout << "-------------------------------------------------------\n";
    std::cout << "INFO: OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Shader: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "INFO: OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "-------------------------------------------------------\n";
}

void InitGLEW() {

    glewExperimental = GL_TRUE;

    if (glewInit() != GLEW_OK) {
        std::cerr << "Threr was a Problem initalizing GLEW\n";
        exit(1);
    }

    if (!GLEW_VERSION_4_4) {
        std::cerr << "OpenGL 4.4 required version support not present\n";
        exit(1);
    }

    std::cout << "Init GLEW done.\n";

}

void InitSDL() {
    // init SDL video system
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Faild to init SDL\n";
        exit(1);
    }

    // create window
    mainWindow = SDL_CreateWindow(
                progName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_OPENGL);

    if (!mainWindow) {
        std::cerr << "Unable to create window\n";
        //TODO: check SDL error
        exit(1);
    }

    // Set OpenGL context profile
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // set OpenGL version (version 3.3)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);

    // set double buffering
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //-------------------------------------------

    // create context
    mainContext = SDL_GL_CreateContext(mainWindow);

    if (!mainContext) {
        std::cerr << "Unable to create context\n";
        //TODO: check SDL error
        Cleanup();
        exit(1);
    }

    // turn (on/off) V-Sync
    SDL_GL_SetSwapInterval(1);

    std::cout << "Init SDL done.\n";

}

void InitGL() {
    // nothing to do here (just for now)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    std::cout << "Init GL done.\n";

}


void InitSphereShader() {
    GLuint vertexShader = Shader::LoadShader(GL_VERTEX_SHADER, "../../res/shader/phong.vert");
    GLuint fragmentShader = Shader::LoadShader(GL_FRAGMENT_SHADER, "../../res/shader/phong.frag");

    std::vector<GLuint> shaders;
    shaders.push_back(vertexShader);
    shaders.push_back(fragmentShader);

    g_ShaderProgram = Shader::CreateShaderProgram(shaders);
    assert(g_ShaderProgram != 0);

    std::cout << "Sphere: Init Shader done.\n";
}



void InitViewport() {

    g_Camera.SetPosition(g_InitialCameraPosition);

    g_Camera.SetViewport(0,0,screenWidth, screenHeight);
    g_Camera.SetProjectionRH(60.0f, screenWidth / (float) screenHeight, 0.1f, 100.0f);

    std::cout << "Init Viewport done.\n";

}

void CreateSphere() {
    g_sphere = new Sphere(1.0f, 32, 32);

    std::cout << "Sphere initialized: " << g_sphere->indicies.size() << " Vertices" << std::endl;

}

void InitSphereVAO() {

    g_uniformMVP = glGetUniformLocation(g_ShaderProgram, "u_mvp");
    g_uniformModelMatrix = glGetUniformLocation(g_ShaderProgram, "u_modelMatrix");

    g_uniformEyePosW = glGetUniformLocation(g_ShaderProgram, "u_eyePosW");
    g_uniformLightPosW = glGetUniformLocation(g_ShaderProgram, "u_lightPosW");
    g_uniformLightColor = glGetUniformLocation(g_ShaderProgram, "u_lightColor");

    g_uniformMaterialEmissive = glGetUniformLocation(g_ShaderProgram, "u_materialEmissive");
    g_uniformMaterialDiffuse = glGetUniformLocation(g_ShaderProgram, "u_materialDiffuse");
    g_uniformMaterialSpecular = glGetUniformLocation(g_ShaderProgram, "u_materialSpecular");
    g_uniformMaterialShininess = glGetUniformLocation(g_ShaderProgram, "u_materialShininess");

    g_uniformAmbient = glGetUniformLocation(g_ShaderProgram, "u_ambient");


    g_uniformRenderTime = glGetUniformLocation(g_ShaderProgram, "u_renderTime");


    GLuint positionAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_position");
    GLuint textcoordAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_texcoord");
    GLuint normalAttribID = (GLuint) glGetAttribLocation(g_ShaderProgram, "in_normal");

    // Create a VAO for the sphere
    glGenVertexArrays(1, &g_vaoSphere);
    glBindVertexArray(g_vaoSphere);


    GLuint vbos[4];
    glGenBuffers(4, vbos);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, g_sphere->positions.size() * sizeof(glm::vec3), g_sphere->positions.data(), GL_STATIC_DRAW );
    glVertexAttribPointer(positionAttribID, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(positionAttribID);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, g_sphere->normals.size() * sizeof(glm::vec3), g_sphere->normals.data(), GL_STATIC_DRAW );
    glVertexAttribPointer(normalAttribID, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(normalAttribID);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, g_sphere->textureCoords.size() * sizeof(glm::vec2), g_sphere->textureCoords.data(), GL_STATIC_DRAW );
    glVertexAttribPointer(textcoordAttribID, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(textcoordAttribID);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, g_sphere->indicies.size() * sizeof(GLuint), g_sphere->indicies.data(), GL_STATIC_DRAW );


    //--------------------------------------

    //Textures pointer
    g_uniformDayMap = glGetUniformLocation(g_ShaderProgram, "dayMap");
    g_uniformNightMap = glGetUniformLocation(g_ShaderProgram, "nightMap");
    g_uniformCloudMap = glGetUniformLocation(g_ShaderProgram, "cloudMap");

    g_uniformSpecularMap = glGetUniformLocation(g_ShaderProgram, "specularMap");



    // Clean up
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(positionAttribID);
    glDisableVertexAttribArray(textcoordAttribID);
    glDisableVertexAttribArray(normalAttribID);

    // cout
    std::cout << "Sphere: VAO + VBO initialized.\n";

}




void InitTexture() {

    // Load Earth
    g_TextureDayMap = Texture::LoadTexture("../../res/textures/8k_earth_daymap.jpg", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);

    g_TextureNightMap = Texture::LoadTexture("../../res/textures/8k_earth_nightmap.jpg", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
    g_TextureCloudMap = Texture::LoadTexture("../../res/textures/8k_earth_clouds.jpg", GL_LINEAR_MIPMAP_LINEAR,  GL_LINEAR_MIPMAP_LINEAR);
    g_TextureSpecularMap = Texture::LoadTexture("../../res/textures/8k_earth_specular_map.png", GL_LINEAR_MIPMAP_LINEAR,  GL_LINEAR_MIPMAP_LINEAR);

    std::cout << "Texture initialized.\n";

}



void Cleanup() {
    SDL_GL_DeleteContext(mainContext);

    SDL_DestroyWindow(mainWindow);

    SDL_Quit();
}

void DrawSphere(float tpf) {
    // clear color buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glBindVertexArray( g_vaoSphere );
    glUseProgram( g_ShaderProgram );

    // Activate Texture-Units
    glUniform1i(g_uniformDayMap, 0);
    glUniform1i(g_uniformNightMap, 1);
    glUniform1i(g_uniformCloudMap, 2);
    glUniform1i(g_uniformSpecularMap, 3);




    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_TextureDayMap);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, g_TextureNightMap);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, g_TextureCloudMap);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, g_TextureSpecularMap);


    //---------------------------------------------------

    glm::mat4 projection_matrix = g_Camera.GetProjectionMatrix();
    glm::mat4 view_matrix = g_Camera.GetViewMatrix();

    //------------------------

    glm::mat4 model_translation = glm::translate(glm::vec3(0));
    glm::mat4 model_scale = glm::scale(glm::vec3(1));
    glm::mat4 model_rotation = glm::toMat4(g_Rotation);

    glm::mat4 model_matrix = model_translation * model_rotation * model_scale;

    //------------------------


    glm::mat4 mvp = projection_matrix * view_matrix * model_matrix;

    //-----------------------------------------------------------


    glUniformMatrix4fv( g_uniformMVP, 1, GL_FALSE, glm::value_ptr(mvp));

    glUniformMatrix4fv( g_uniformModelMatrix, 1, GL_FALSE, glm::value_ptr(model_matrix));




    glm::vec4 eyePosW = glm::vec4(g_Camera.GetPosition(), 1);
    glUniform4fv( g_uniformEyePosW, 1, glm::value_ptr(eyePosW));

    glm::vec4 lightPosW = glm::vec4(g_SunPosition, 1);
    glUniform4fv( g_uniformLightPosW, 1, glm::value_ptr(lightPosW));

    glUniform4fv( g_uniformLightColor, 1, glm::value_ptr(g_lightColor));


    glUniform4fv( g_uniformAmbient, 1, glm::value_ptr(g_ambientColor));


    glUniform4fv( g_uniformMaterialEmissive, 1, glm::value_ptr(g_emissiveColor));
    glUniform4fv( g_uniformMaterialDiffuse, 1, glm::value_ptr(g_diffuseColor));
    glUniform4fv( g_uniformMaterialSpecular, 1, glm::value_ptr(g_specularColor));

    glUniform1f(g_uniformMaterialShininess, g_shininess);


    // pass render time
    Uint32 current_time = SDL_GetTicks();
    float renderTime = current_time / 100000.0f;
    glUniform1f(g_uniformRenderTime, renderTime);


    glDrawElements(GL_TRIANGLES, g_sphere->indicies.size(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));


    // clean up
    glUseProgram(0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);


    // swap back and front buffer (show frame)
    SDL_GL_SwapWindow(mainWindow);

    // inc FrameCount
    frameCount++;


}


void MoveObjects(float tpf) {

    float speed = 0.005f * tpf;

    g_Camera.Translate(glm::vec3((g_A - g_D), (g_S - g_W),0) * speed);


    if (!g_M1) {

        //glm::vec3 axis_x = glm::normalize(glm::vec3(1,0,0) * glm::toMat3(g_Rotation));
        glm::vec3 axis_y = glm::normalize(glm::vec3(0,1,0) * glm::toMat3(g_Rotation));

        //glm::quat rot_x = glm::angleAxis<float>(0.0005f * tpf, axis_x);
        glm::quat rot_y = glm::angleAxis<float>(0.0003f * tpf, axis_y);

        g_Rotation = g_Rotation * rot_y;

    }


    // Rotate sun
    g_SunPosition = g_SunPosition * g_SunRotation;



}

Uint32 CalcFPS(Uint32 interval, void *param) {

    // Calc avg fps
    avgFps = frameCount / (interval / 1000.0f);

    // reset frame count
    frameCount = 0;

    // Set window title with fps
    std::stringstream winTitle;

    winTitle << progName << " @ " << std::fixed << std::setprecision(1) << avgFps << " fps";

    SDL_SetWindowTitle(mainWindow, winTitle.str().c_str());

    timerID = SDL_AddTimer(interval, CalcFPS, param);

    return 0;

}

void SDLKeyboardHandler(SDL_Keysym key, int down) {
    switch(key.sym) {
        case SDLK_w:
            g_W = down;
            break;
        case SDLK_a:
            g_A = down;
            break;
        case SDLK_s:
            g_S = down;
            break;
        case SDLK_d:
            g_D = down;
            break;
        case SDLK_r:
            g_Camera.SetPosition(g_InitialCameraPosition);
           // g_Camera.SetRotation(g_InitialCameraRotation);
            g_Rotation = QUAT_IDENTITY;
            break;
        case SDLK_ESCAPE:
            stopRenderLoop = true;
            break;
        default:
            break;
    }
}

void MouseMoveHandler(SDL_MouseMotionEvent event) {

    if (g_M2) {

        float speed = 0.01f;

        g_Camera.Translate(glm::vec3(-event.xrel, event.yrel, 0) * speed);
    }

    if (g_M1) {

        glm::vec3 axis_x = glm::normalize(glm::vec3(1,0,0) * glm::toMat3(g_Rotation));
        glm::vec3 axis_y = glm::normalize(glm::vec3(0,1,0) * glm::toMat3(g_Rotation));

        glm::quat rot_x = glm::angleAxis<float>(0.01 * event.yrel, axis_x);
        glm::quat rot_y = glm::angleAxis<float>(0.01 * event.xrel, axis_y);

        g_Rotation = g_Rotation * rot_x * rot_y;
    }

    if (g_M3) {

        //glm::vec3 axis_x = glm::normalize(glm::vec3(1,0,0) * glm::toMat3(g_Rotation));
       // glm::vec3 axis_y = glm::normalize(glm::vec3(0,1,0) * glm::toMat3(g_Rotation));

        glm::quat rot_x = glm::angleAxis<float>(0.0001 * event.yrel, glm::vec3(1,0,0));
        glm::quat rot_y = glm::angleAxis<float>(0.0001 * event.xrel, glm::vec3(0,1,0));

        g_SunRotation = g_SunRotation * rot_x * rot_y;
    }



}

void MouseWheelHandler(SDL_MouseWheelEvent event) {

    float cam_dist = g_Camera.GetPosition().z;

    float factor = (cam_dist - 1.1f) * 0.01f;

    g_Camera.Translate(glm::vec3(0,0, -event.y * factor));

}



void Render() {

    timerID = SDL_AddTimer(3 * 1000 , CalcFPS, nullptr);

    Uint64 now = SDL_GetPerformanceCounter();
    Uint64 last = 0;
    float tpf = 0;

     while (!stopRenderLoop) {

        SDL_Event event;
        while (SDL_PollEvent(&event)) {

            if (event.type == SDL_QUIT) stopRenderLoop = true;

            if (event.type == SDL_KEYDOWN && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 1);
            }

            if (event.type == SDL_KEYUP && event.key.repeat == 0) {
                SDLKeyboardHandler(event.key.keysym, 0);
            }

            if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (event.button.button == SDL_BUTTON_LEFT) g_M1 = 1;
                if (event.button.button == SDL_BUTTON_RIGHT) g_M2 = 1;
                if (event.button.button == SDL_BUTTON_MIDDLE) g_M3 = 1;

            }

            if (event.type == SDL_MOUSEBUTTONUP) {
                if (event.button.button == SDL_BUTTON_LEFT) g_M1 = 0;
                if (event.button.button == SDL_BUTTON_RIGHT) g_M2 = 0;
                if (event.button.button == SDL_BUTTON_MIDDLE) g_M3 = 0;
            }

            if (event.type == SDL_MOUSEMOTION)
                MouseMoveHandler(event.motion);

            if (event.type == SDL_MOUSEWHEEL)
                MouseWheelHandler(event.wheel);

        }

        // Move objects
        MoveObjects(tpf);

        // draw the scene
        DrawSphere(tpf);


        // calc tpf
        last = now;
        now = SDL_GetPerformanceCounter();
        tpf = ((now - last) * 1000.0 /(float) SDL_GetPerformanceFrequency());
    }
}

int main(int argc, char *argv[]) {

    InitSDL();

    InitGLEW();

    InitGL();

    //------------------------------------------

    CreateSphere();

    InitSphereShader();

    InitSphereVAO();

    InitTexture();

    //------------------------------------------

    InitViewport();

    PrintOpenGLVersion();

    std::cout << "Rendering...\n";

    Render();

    std::cout << "Rendering done, bye.\n";

    Cleanup();

    return 0;
}

