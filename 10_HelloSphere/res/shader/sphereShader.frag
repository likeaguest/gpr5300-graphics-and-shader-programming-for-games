#version 440

in vec2 out_texcoord;

uniform sampler2D surface;

out vec4 fragColor;

void main() {
    fragColor = texture(surface, out_texcoord);
}
