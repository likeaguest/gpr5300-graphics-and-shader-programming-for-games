#version 440

// Input color from the vertex program.
in vec4 v2f_color;
in vec3 v2f_position;

uniform samplerCube skybox;

out vec4 out_color;


void main()
{

    //out_color = vec4(1,0,0,1);

   // out_color = v2f_color;

    out_color = texture(skybox, v2f_position);

}