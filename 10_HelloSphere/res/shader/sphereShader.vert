#version 440

in vec3 in_position;
in vec3 in_normal;
in vec2 in_texcoord;

uniform mat4 u_mvp;

out vec2 out_texcoord;

void main() {
    out_texcoord = in_texcoord;
    gl_Position = u_mvp * vec4(in_position, 1.0);
}
