#include "HelloSphere.h"
#include "Sphere.h"


Sphere::Sphere( float radius, int slices, int stacks ) {

    for( int i = 0; i <= stacks; ++i ) {

        // V texture coordinate.
        float V = i / (float)stacks;
        float phi = V * pi;

        for ( int j = 0; j <= slices; ++j ) {

            // U texture coordinate.
            float U = j / (float)slices;
            float theta = U * _2pi;

            float X = cos(theta) * sin(phi);
            float Y = cos(phi);
            float Z = sin(theta) * sin(phi);

            positions.push_back( vec3( X, Y, Z) * radius );
            normals.push_back( vec3(X, Y, Z) );
            //textureCoords.push_back( vec2(U, V) );

            textureCoords.push_back( vec2(1.0f - U, V) );



        }

    }

    for( GLuint i = 0; i < slices * stacks + slices; ++i ) {

        indices.push_back( i );
        indices.push_back( i + slices + 1 );
        indices.push_back( i + slices );

        indices.push_back( i + slices + 1 );
        indices.push_back( i );
        indices.push_back( i + 1 );

    }




}
